package com.example.hw20

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import retrofit2.*
import java.io.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import hw20.databinding.FragmentObservableBinding
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import kotlin.random.Random

class Observable : Fragment() {
    private val baseUrl = "https://jsonplaceholder.typicode.com/"
    private lateinit var binding: FragmentObservableBinding
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentObservableBinding.inflate(inflater)
        binding.fromAPI.setOnClickListener {
            try {
                getDataFromWeb()
                binding.status.text = "Downloaded"
            }catch (e:IOException){
                binding.status.text = "Download failed"
            }

        }
        binding.fromFile.setOnClickListener {
            try {
                getDataFromJSON()
                binding.status.text = "Imported"
            }catch (e:IOException){
                binding.status.text = "Import failed"
            }
        }
        return binding.root
    }

    private fun getDataFromWeb() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()
            .create(APIInterface::class.java)

        val retrofitData = retrofitBuilder.getData()
        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            override fun onResponse(
                call: Call<List<MyDataItem>?>,
                response: Response<List<MyDataItem>?>
            ) {
                val responseBody = response.body()!!
                CurrentPub.pub.publish(getRandomString(responseBody))
            }
            override fun onFailure(call: Call<List<MyDataItem>?>, t: Throwable) {
            }
        })
    }

    private fun getDataFromJSON(){
        try {
            val path = context?.filesDir
            val file = File(path, "/src/res/raw/data.json")
            val fileReader = FileReader(file)
            val bufferedReader = BufferedReader(fileReader)
            val stringBuilder = StringBuilder()
            var line: String = bufferedReader.readLine()
            while (line != null) {
                stringBuilder.append(line).append("\n")
                line = bufferedReader.readLine()
            }
            bufferedReader.close()
            val response = stringBuilder.toString()
            val listType: Type = object: TypeToken<List<MyDataItem>>(){}.type
            val listOfMessage:List<MyDataItem> = Gson().fromJson(response, listType)
            CurrentPub.pub.publish(getRandomString(listOfMessage))

        }catch (e:IOException){
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
            throw e
        }
    }


    fun getRandomString(mList:List<MyDataItem>):String{
        val i:Int = Random.nextInt(0, mList.size-1)
        return mList[i].body

    }

    companion object {
        @JvmStatic
        fun newInstance() = Observable()
    }
}