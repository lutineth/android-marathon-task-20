package com.example.hw20

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import hw20.databinding.FragmentObserverBinding
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable

class Observer : Fragment() {

    lateinit var binding:FragmentObserverBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentObserverBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.button.setOnClickListener {
            CurrentPub.pub.listen().subscribe(createObs())
        }
    }

    private fun createObs():Observer<String>{
        val observer:Observer<String> = object: Observer<String>{
            @SuppressLint("SetTextI18n")
            override fun onSubscribe(d: Disposable) {
                binding.textView.text = "Subscribed"
            }
            override fun onNext(t: String) {
                binding.textView.text = t
            }

            @SuppressLint("SetTextI18n")
            override fun onError(e: Throwable) {
                binding.textView.text = "Error"
            }

            @SuppressLint("SetTextI18n")
            override fun onComplete() {
                binding.textView.text = "Completed"
            }
        }
        return observer
    }

    companion object {
        @JvmStatic
        fun newInstance() = Observer()
    }
}