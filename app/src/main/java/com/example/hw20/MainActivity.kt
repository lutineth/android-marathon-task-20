package com.example.hw20

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import hw20.R
import hw20.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private var current:Int = R.id.start_page
    private lateinit var binding:ActivityMainBinding
    private var fragmentManager:FragmentManager = supportFragmentManager
    private var obsrvr:Observer? = null
    private var obsrvbl:Observable?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        setContentView(binding.root)
        obsrvr = Observer()
        fragmentManager.beginTransaction().add(R.id.fragmentShow, obsrvr!!).commit()
        binding.navMenu.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.download-> {
                    if(obsrvbl!=null){
                        fragmentManager.beginTransaction().hide(obsrvr!!).show(obsrvbl!!).commit()
                        current = it.itemId
                    }
                    if(obsrvbl == null){
                        obsrvbl = Observable()
                        fragmentManager.beginTransaction().add(R.id.fragmentShow, obsrvbl!!).hide(obsrvr!!).commit()
                    }
                }
                R.id.start_page ->{
                        if (obsrvr != null) {
                            fragmentManager.beginTransaction().hide(obsrvbl!!).show(obsrvr!!).commit()
                            current = it.itemId
                        }
                }
            }
            true
        }
    }
}