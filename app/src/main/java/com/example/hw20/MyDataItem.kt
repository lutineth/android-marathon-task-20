package com.example.hw20

data class MyDataItem(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String
)