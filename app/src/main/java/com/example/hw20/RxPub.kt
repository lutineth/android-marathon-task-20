package com.example.hw20

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject

open class RxPub {
    var mInstance:RxPub? = null
    private val publisher:PublishSubject<String> = PublishSubject.create()
    fun getInstance():RxPub{
        if(mInstance == null){
            mInstance = RxPub()
        }
        return mInstance!!
    }

    fun publish(event:String){
        publisher.onNext(event)
    }

    fun listen():Observable<String>{
        return publisher
    }
}